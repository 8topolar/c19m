const http = require( 'http' )
const map = require( './map' )

// environment variables
const { PORT = 3000 } = process.env

// app
const app = http.createServer( map )

app.listen( PORT, () => {
  console.log( `magic happends on ${ PORT }` )
} )
