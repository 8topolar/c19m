const csv = require( 'csv-parser' )
const fs = require( 'fs' )
const moment = require( 'moment' ).utc
const path = require( 'path' )
const simpleGit = require( 'simple-git' )

// environment variables
const {
  BRANCH = 'web-data',
  LAST_UPDATED_FILEPATH = path.join( process.cwd(), '.last-updated' )
} = process.env

// global variables
const cwd = path.resolve( process.cwd() )

// helpers
async function csv2json( filepath ) {
  return new Promise( ( resolve, reject ) => {
    const results = []
    fs.createReadStream( filepath )
      .pipe( csv() )
      .on( 'data', data => results.push( data ) )
      .on( 'end', () => {
        return resolve( results )
      } )
      .on( 'error', reject )
  } )
}

function saveSnap( filepath, data ) {
  fs.writeFileSync( filepath, data )
}

function loadLastUpdated() {
  try {
    const lastUpdate = fs.readFileSync( LAST_UPDATED_FILEPATH ).toString()
    return moment( Number.parseInt( lastUpdate ) )
  } catch ( e ) {
    return moment()
  }
}

function formatDate( date, format = 'YYYY-MM-DD' ) {
  if ( date instanceof Date === false ) throw new Error( 'date object must be instance of Date' )

  console.log( 'year', format.match( /(Y)/ ) )
}

// git
const dbDir = path.join( cwd, 'db' )

const gitOptions = {
  baseDir: dbDir,
  binary: 'git',
  maxConcurrentProcesses: 6
}

const git = simpleGit( gitOptions )

async function snap( options = {} ) {
  return new Promise( async ( resolve, reject ) => {
    // dates
    const currentDate = moment()
    const lastUpdate = loadLastUpdated()

    if ( ! /\d{4}(\-\d{2}){2}/.test( options.date ) )
      return reject( 'invalid date format' )

    // check branch
    const branch = await git.branch()
    if ( branch.current !== BRANCH ) await git.checkout( [ BRANCH ] )

    // update branch
    if ( options.date === currentDate.format( 'YYYY-MM-DD' ) ) {
      const diff = currentDate.diff( lastUpdate )
      if ( diff === -1 || diff > 3600000 ) {
        await git.pull()
        fs.writeFileSync( LAST_UPDATED_FILEPATH, currentDate.format( 'x' ) )
      }
    }

    // format input
    const day = options.date
    const re = new RegExp( `^${day}.*` )

    const before = day + 'T23:59:59+000Z'

    let log = await git.log( [
      `--before="${ before }"`,
      '-1'
    ] )

    let latest = log.all
      .filter( el => re.test( el.date ) )
      .shift()

    if ( ! latest ) {
      log = await git.log( [
        '-1'
      ] )
      latest = log.all.shift()
    }

    const snapFilePath = path.join( cwd, 'snap', `${ latest.hash }.json` )

    if ( fs.existsSync( snapFilePath ) ) {
      // const data = JSON.parse( fs.readFileSync( snapFilePath ).toString() )
      const data = require( snapFilePath )
      return resolve( data )
    }

    // recovery data files
    await git.checkout( [ latest.hash, '--', 'data/cases*.csv' ] )

    // parse recovered data
    const byCountry = await csv2json( path.join( cwd, 'db', 'data', 'cases_country.csv' ) )
    const byState = await csv2json( path.join( cwd, 'db', 'data', 'cases_state.csv' ) )

    // build data
    const data = {
      date: day,
      byCountry,
      byState
    }

    // save snap
    saveSnap( snapFilePath, JSON.stringify( data, null, 2 ) )

    // reset
    await git.reset( [ '--', 'data' ] )
    await git.checkout( [ 'data' ] )

    return resolve( data )
  } )
}

module.exports = snap
