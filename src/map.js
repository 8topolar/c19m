const cheerio = require( 'cheerio' )
const DottedMap = require( 'dotted-map' )
const fs = require( 'fs' )
const moment = require( 'moment' ).utc
const path = require( 'path' )
const qs = require( 'qs' )
const snap = require( './snap' )

const regions = require( 'dotted-map/countries.geo.json' ).features
  .map( el => ( {
    id: el.id,
    name: el.properties.name
  } ) )

// helpers
function commaify( n ) {
  return n.toString().replace( /\B(?=(\d{3})+(?!\d))/g, ',' )
}

function bodyParser( request ) {
  const { 'content-type': contentType } = request.headers
  const { bodyString } = request
  let parsedBody
  switch ( contentType ) {
    case 'application/x-www-form-urlencoded':
      parsedBody = qs.parse( bodyString )
      break
    case 'application/json':
      try {
        parsedBody = JSON.parse( bodyString )
      } catch ( e ) {}
      break
  }
  return parsedBody || {}
}

// global variables
const TEMPLATE = ''.concat( fs.readFileSync( path.join ( 'src', 'template.html' ) ) )

const defaultMapOptions = {
  height: 60,
  grid: 'diagonal',
  avoidOuterPins: true
}

async function map( req, res ) {
  req.bodyString = ''

  req.on( 'data', data => {
    req.bodyString += data
  } )

  req.on( 'end', () => {
    // not root
    if ( req.url !== '/' ) {
      res.writeHead( 404 )
      return res.end()
    }

    // parse body
    const body = bodyParser( req )

    // recovery params from body
    const {
      date = moment().format( 'YYYY-MM-DD' ),
      map = {},
      shape = 'circle',
      radius = '0.22',
      color = '#007700',
      backgroundColor = '#001100'
    } = body

    let region = 'global'

    snap( { date } )
      .then( snapData => {
        // countries vs states
        // country headers
        // Country_Region,Last_Update,Lat,Long_,Confirmed,Deaths,Recovered,Active,Incident_Rate,People_Tested,People_Hospitalized,Mortality_Rate,UID,ISO3
        // state headers
        // Province_State,Country_Region,Last_Update,Lat,Long_,Confirmed,Deaths,Recovered,Active,FIPS,Incident_Rate,People_Tested,People_Hospitalized,Mortality_Rate,UID,ISO3,Testing_Rate,Hospitalization_Rate
        let data = snapData.byCountry
        if ( map.countries ) data = snapData.byState
        const pins = data
          .map( el => ( {
            lat: Number.parseFloat( el.Lat ),
            lng: Number.parseFloat( el.Long_ ),
            data: [
              'name:' + ( map.countries ? el.Province_State : el.Country_Region ),
              'confirmed:' + parseFloat( el.Confirmed ),
              'deaths:' + parseFloat( el.Deaths ),
              'recovered:' + parseFloat( el.Recovered ),
              'active:' + parseFloat( el.Active )
            ].join( '|' ),
            svgOptions: {
              color: '#aa0000',
              radius: 0.4
            }
          } ) )
          .filter( el => !Number.isNaN( el.lat ) )

        const dottedMap = new DottedMap( { ...defaultMapOptions, ...map } )

        pins.forEach( pin => {
          dottedMap.addPin( pin )
        } )

        // recovery markers
        const markers = dottedMap.getPoints()
          .filter( el => el.data )

        // format svg
        const svgMap = dottedMap.getSVG( { shape, radius, color, backgroundColor } )

        const $svg = cheerio.load( svgMap )
        $svg( 'svg' ).attr( 'id', 'svg-map' )
        const $g = cheerio.load( '<g id="mapGroup" transform="matrix(1 0 0 1 0 0)"></g>' )
        $g( 'g' ).append( $svg( 'svg' ).html() )
        $svg( 'svg' ).html( '' )
        $svg( 'svg' ).append( $g( 'g' ) )

        // add marker class to pin
        markers
          .forEach( el => {
            const item = $svg( `[cx="${ el.x }"][cy="${ el.y }"]` )
            item.addClass( 'marker' )
            item.attr( 'data-marker', el.data )
            item.attr( 'title', el.data )
          } )

        const svg = $svg( 'body' ).html()

        // region
        if ( map.countries ) {
          region = map.countries
            .map( c => regions.filter( r => r.id === c ).shift() )
            .map( c => c.name )
            .join( ', ' )
        }

        // totals
        const totals = markers
          .reduce( ( totals, el ) => {
            const parsedData = el.data
              .split( '|' )
              .map( el => el.split( ':' ).pop() )
              .map( el => Number.parseFloat( el ) || 0 )

            totals.confirmed += parsedData[ 1 ]
            totals.deaths += parsedData[ 2 ]
            totals.recovered += parsedData[ 3 ]
            totals.active += parsedData[ 4 ]

            return totals
          }, {
            confirmed: 0,
            deaths: 0,
            recovered: 0,
            active: 0
          } )

        // build template
        let content = TEMPLATE
        content = content.replace( '<% REGION %>', region )
        content = content.replace( '<% CONFIRMED %>', commaify( totals.confirmed ) )
        content = content.replace( '<% DEATHS %>', commaify( totals.deaths ) )
        content = content.replace( '<% RECOVERED %>', commaify( totals.recovered ) )
        content = content.replace( '<% ACTIVE %>', commaify( totals.active ) )
        content = content.replace( '<% SVG %>', svg )
        content = content.replace( '<% REGIONS %>', JSON.stringify( regions ) )

        res.writeHead( 200, { 'Content-Type': 'text/html' } )
        res.write( content )
        res.end()
      } )
      .catch( err => {
        console.error( 'ERR', err )
        res.writeHead( 500, { 'Content-Type': 'text/plain' } )
        res.write( err )
        res.end()
      } )

  } )
}

module.exports = map
